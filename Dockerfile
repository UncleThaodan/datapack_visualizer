FROM python:3.12-alpine
WORKDIR /datapack_visualizer/

# Update system
RUN apk update && apk upgrade --available && apk add --no-cache graphviz xdg-utils

# Install application
RUN pip install dpvisu

# ---------------------------------------------------------------------------- #
#          Copy datapacks. Gitlab does this as part of the pipeline.
#            Uncomment to build the image for your own, local use.
# ---------------------------------------------------------------------------- #
#COPY my_datapack_path/ .

# Default run
CMD ["dpvisu", ".", "-o", "generated_overview"]
